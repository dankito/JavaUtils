package net.dankito.utils.datetime

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*


@JvmName("asNullableLocalDate")
fun Date?.asLocalDate(zone: ZoneId = ZoneId.systemDefault()): LocalDate? {
    return DateConvertUtils.asLocalDate(this, zone)
}

fun Date.asLocalDate(zone: ZoneId = ZoneId.systemDefault()): LocalDate {
    return DateConvertUtils.asLocalDate(this, zone)
}


@JvmName("asNullableLocalDateTime")
fun Date?.asLocalDateTime(zone: ZoneId = ZoneId.systemDefault()): LocalDateTime? {
    return DateConvertUtils.asLocalDateTime(this, zone)
}

fun Date.asLocalDateTime(zone: ZoneId = ZoneId.systemDefault()): LocalDateTime {
    return DateConvertUtils.asLocalDateTime(this, zone)
}