package net.dankito.utils.datetime

import java.time.LocalDateTime
import java.util.*


@JvmName("asNullableUtilDate")
fun LocalDateTime?.asUtilDate(): Date? {
    return DateConvertUtils.asUtilDate(this)
}

fun LocalDateTime.asUtilDate(): Date {
    return DateConvertUtils.asUtilDate(this)
}