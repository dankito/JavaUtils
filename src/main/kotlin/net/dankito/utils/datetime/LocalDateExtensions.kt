package net.dankito.utils.datetime

import java.time.LocalDate
import java.util.*


@JvmName("asNullableUtilDate")
fun LocalDate?.asUtilDate(): Date? {
    return DateConvertUtils.asUtilDate(this)
}

fun LocalDate.asUtilDate(): Date {
    return DateConvertUtils.asUtilDate(this)
}