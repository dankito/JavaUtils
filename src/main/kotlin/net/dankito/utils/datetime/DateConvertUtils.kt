package net.dankito.utils.datetime

import java.time.*
import java.util.*


/**
 * Utilities for conversion between the old and new JDK date types
 * (between `java.util.Date` and `java.time.*`).
 *
 * All methods are null-safe.
 *
 *
 * Thanks to Oliv (https://stackoverflow.com/a/27378709) for providing this class.
 *
 */
object DateConvertUtils {

    val UtcZoneId = ZoneId.of("UTC")


    /**
     * Creates [LocalDate] from `java.util.Date` or it's subclasses.
     *
     * Returns null if [date] is null.
     */
    @JvmOverloads
    @JvmName("asNullableLocalDate")
    fun asLocalDate(date: Date?, zone: ZoneId = UtcZoneId): LocalDate? {
        if (date == null)
            return null

        return asLocalDate(date, zone)
    }

    /**
     * Creates [LocalDate] from `java.util.Date` or it's subclasses.
     *
     * Null is not allowed for [date].
     */
    @JvmOverloads
    fun asLocalDate(date: Date, zone: ZoneId = UtcZoneId): LocalDate {
        return if (date is java.sql.Date)
            date.toLocalDate()
        else
            Instant.ofEpochMilli(date.time).atZone(zone).toLocalDate()
    }

    /**
     * Creates [LocalDateTime] from `java.util.Date` or it's subclasses.
     *
     * Returns null if [date] is null.
     */
    @JvmOverloads
    @JvmName("asNullableLocalDateTime")
    fun asLocalDateTime(date: Date?, zone: ZoneId = UtcZoneId): LocalDateTime? {
        if (date == null)
            return null

        return asLocalDateTime(date, zone)
    }

    /**
     * Creates [LocalDateTime] from `java.util.Date` or it's subclasses.
     *
     * Null is not allowed for [date].
     */
    @JvmOverloads
    fun asLocalDateTime(date: Date, zone: ZoneId = UtcZoneId): LocalDateTime {
        return if (date is java.sql.Timestamp)
            date.toLocalDateTime()
        else
            Instant.ofEpochMilli(date.time).atZone(zone).toLocalDateTime()
    }


    /**
     * Creates a [Date] from various date objects. Is null-safe. Currently supports:
     *  * [Date]
     *  * [java.sql.Date]
     *  * [java.sql.Timestamp]
     *  * [java.time.LocalDate]
     *  * [java.time.LocalDateTime]
     *  * [java.time.ZonedDateTime]
     *  * [java.time.Instant]
     *
     *
     * @param zone Time zone, used only if the input object is LocalDate or LocalDateTime.
     *
     * @return [Date] (exactly this class, not a subclass, such as java.sql.Date) or null if [date] is null.
     */
    @JvmOverloads
    @JvmName("asNullableUtilDate")
    fun asUtilDate(date: Any?, zone: ZoneId = UtcZoneId): Date? {
        if (date == null)
            return null

        return asUtilDate(date, zone)
    }

    /**
     * Creates a [Date] from various date objects. Null is **not** allowed for [date]. Currently supports:
     *  * [Date]
     *  * [java.sql.Date]
     *  * [java.sql.Timestamp]
     *  * [java.time.LocalDate]
     *  * [java.time.LocalDateTime]
     *  * [java.time.ZonedDateTime]
     *  * [java.time.Instant]
     *
     *
     * @param zone Time zone, used only if the input object is LocalDate or LocalDateTime.
     *
     * @return [Date] (exactly this class, not a subclass, such as java.sql.Date)
     */
    @JvmOverloads
    fun asUtilDate(date: Any, zone: ZoneId = UtcZoneId): Date {

        when (date) {
            is java.sql.Date, is java.sql.Timestamp -> return Date((date as Date).time)
            is Date -> return date
            is LocalDate -> return Date.from(date.atStartOfDay(zone).toInstant())
            is LocalDateTime -> return Date.from(date.atZone(zone).toInstant())
            is ZonedDateTime -> return Date.from(date.toInstant())
            is Instant -> return Date.from((date as Instant?)!!)
            else -> throw UnsupportedOperationException("Don't know hot to convert " + date.javaClass.name + " to java.util.Date")
        }

    }


    /**
     * Creates an [Instant] from `java.util.Date` or it's subclasses. Null-safe.
     */
    @JvmName("asNullableInstant")
    fun asInstant(date: Date?): Instant? {
        return if (date == null)
            null
        else
            Instant.ofEpochMilli(date.time)
    }

    /**
     * Creates an [Instant] from `java.util.Date` or it's subclasses. Null is not allowed for [date].
     */
    fun asInstant(date: Date): Instant {
        return Instant.ofEpochMilli(date.time)
    }


    /**
     * Creates [ZonedDateTime] from `java.util.Date` or it's subclasses. Null-safe.
     */
    @JvmOverloads
    @JvmName("asNullableZonedDateTime")
    fun asZonedDateTime(date: Date?, zone: ZoneId = UtcZoneId): ZonedDateTime? {
        return if (date == null)
            null
        else
            asInstant(date).atZone(zone)
    }

    /**
     * Creates [ZonedDateTime] from `java.util.Date` or it's subclasses. Null is not allowed for [date].
     */
    @JvmOverloads
    fun asZonedDateTime(date: Date, zone: ZoneId = UtcZoneId): ZonedDateTime {
        return asInstant(date).atZone(zone)
    }

}
