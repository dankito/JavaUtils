package net.dankito.utils.extensions

import java.text.Collator
import java.util.*


class CollectionsExtensions {
    companion object {
        val collator = Collator.getInstance()

        init {
            collator.strength = Collator.IDENTICAL
        }
    }
}


fun <T> Collection<T>.containsAny(otherCollection: Collection<T>): Boolean {
    for (otherItem in otherCollection) {
        if (this.contains(otherItem)) {
            return true
        }
    }

    return false
}

fun <T> Collection<T>.containsExactly(vararg items: T): Boolean {
    return containsExactly(items.toList())
}

fun <T> Collection<T>.containsExactly(otherCollection: Collection<T>): Boolean {
    if (this.size != otherCollection.size) {
        return false
    }

    for (otherItem in otherCollection) {
        if (this.contains(otherItem) == false) {
            return false
        }
    }

    return true
}


/**
 * Standard sortedBy() function doesn't take characters like German Umlaute into consideration (so that e.g. Ärzte is ordered after Zucker)
 * -> use a Collator with at least strength of Collator.SECONDARY
 */
fun <T> Iterable<T>.sortedByStrings(selector: (T) -> String): List<T> {
    return sortedWith(kotlin.Comparator { o1, o2 -> CollectionsExtensions.collator.compare(selector(o1), selector(o2)) })
}


fun <T> Collection<T>.didCollectionChange(collectionToCompareTo: Collection<T>): Boolean {
    if(this.size != collectionToCompareTo.size) {
        return true
    }

    val copy = ArrayList(collectionToCompareTo)
    copy.removeAll(this)
    return copy.size > 0
}