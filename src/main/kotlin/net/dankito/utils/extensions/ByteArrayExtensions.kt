package net.dankito.utils.extensions


fun ByteArray.toHexString(): String {
    val hexString = StringBuilder()

    for (index in 0 until this.size) {
        hexString.append(String.format("%02x", (0xFF and this[index].toInt())))
    }

    return hexString.toString()
}