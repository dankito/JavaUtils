package net.dankito.utils.extensions

import net.dankito.utils.html.toPlainText
import org.jsoup.Jsoup


fun String.ofMaxLength(maxLength: Int): String {
    if(this.length > maxLength && maxLength > 0) {
        return this.substring(0, maxLength)
    }

    return this
}


fun String.countOccurrences(charToFind: Char): Int {
    var countOccurrences = 0

    for (char in this) {
        if (char == charToFind) {
            countOccurrences++
        }
    }

    return countOccurrences
}


fun String.allIndicesOf(toFind: String): List<Int> {
    val indices = mutableListOf<Int>()
    var index = -1

    do {
        index = this.indexOf(toFind, index + 1)

        if (index > -1) {
            indices.add(index)
        }
    } while (index > -1)

    return indices
}


/**
 * Converts given html String to plain text.
 *
 * Be aware that for this method you need to add dependency "org.jsoup:jsoup" to your classpath!
 */
fun String.htmlToPlainText(): String {
    return Jsoup.parse(this).toPlainText()
}