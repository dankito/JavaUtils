package net.dankito.utils.extensions

import net.dankito.utils.io.FileFileInfoWrapper
import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.JavaIoFileInfo
import java.io.File


fun File.toFileInfo(): FileInfo {
    if (this is FileFileInfoWrapper) {
        return this.fileInfo
    }
    else {
        return JavaIoFileInfo(this)
    }
}