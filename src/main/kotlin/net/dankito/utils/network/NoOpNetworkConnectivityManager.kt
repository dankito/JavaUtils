package net.dankito.utils.network

import java.net.Inet4Address


class NoOpNetworkConnectivityManager : INetworkConnectivityManager {

    override fun getBroadcastAddresses(): Collection<Inet4Address> {
        return emptyList()
    }

    override fun addNetworkInterfaceConnectivityChangedListener(listener: (NetworkInterfaceState) -> Unit) {

    }

    override fun removeNetworkInterfaceConnectivityChangedListener(listener: (NetworkInterfaceState) -> Unit) {

    }

}