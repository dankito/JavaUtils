package net.dankito.utils.network

import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface


interface INetworkHelper {
    // in IPv6 there's no such thing as broadcast
    fun getBroadcastAddress(networkInterface: NetworkInterface): Inet4Address?

    fun getBroadcastAddress(address: Inet4Address): Inet4Address?
    /**
     * Returns MAC address of the given interface name.
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * *
     * @return  mac address or empty string
     */
    fun getMACAddress(interfaceName: String?): String

    /**
     * Get IP address from first non-localhost interface
     * @param useIPv4  true=return ipv4, false=return ipv6
     * *
     * @return  address or empty string
     */
    fun getIPAddress(useIPv4: Boolean): InetAddress?

    fun getIPAddresses(onlyIPv4: Boolean): List<InetAddress>
    fun getIPAddresses(networkInterface: NetworkInterface, onlyIPv4: Boolean): List<InetAddress>
    fun getRealNetworkInterfaces(): Collection<NetworkInterface>
    fun getConnectedRealNetworkInterfaces(): Collection<NetworkInterface>
    fun isSocketCloseException(exception: Exception): Boolean

    fun isTcpPortAvailable(port: Int): Boolean
    fun isUdpPortAvailable(port: Int): Boolean

}