package net.dankito.utils.network

import java.util.*
import kotlin.concurrent.schedule


open class PeriodicCheckingNetworkConnectivityManager(networkHelper: INetworkHelper)
    : NetworkConnectivityManagerBase(networkHelper) {

    companion object {
        private const val CheckForNetworkInterfaceChangesPeriodMillis = 60 * 1000L
    }


    init {
        Timer().schedule(CheckForNetworkInterfaceChangesPeriodMillis, CheckForNetworkInterfaceChangesPeriodMillis) { networkInterfacesChanged() }
    }

}