package net.dankito.utils.clipboard


data class ClipboardContentOption(val title: String, private val action: (ClipboardContentOption) -> Unit) {

    companion object {
        val ActionStartProgress = 0f
        val ActionDoneProgress = 100f

        val IndeterminateProgress = Float.MAX_VALUE
    }


    private var currentProgress = ActionStartProgress

    private val isExecutingListeners = mutableListOf<(progress: Float) -> Unit>()


    val isExecuting: Boolean
        get() {
            return currentProgress == IndeterminateProgress ||
                    (currentProgress >= 0f && currentProgress < ActionDoneProgress)
        }

    val isDone: Boolean
        get() {
            return currentProgress != IndeterminateProgress &&
                    (currentProgress >= ActionDoneProgress || currentProgress < 0.0) // < 0.0 == error
        }

    val progressString: String
        get() {
            return  if(currentProgress >= 0.0 && currentProgress <= ActionDoneProgress) String.format("%.1f", currentProgress) + " %"
                    else ""
        }


    fun updateIsExecutingState(progress: Float) {
        currentProgress = progress

        isExecutingListeners.forEach { listener ->
            listener(progress)
        }
    }

    fun setIndeterminateProgressState() {
        updateIsExecutingState(IndeterminateProgress)
    }

    fun setActionDone() {
        updateIsExecutingState(ActionDoneProgress)
    }



    fun addIsExecutingListener(listener: (progress: Float) -> Unit) {
        isExecutingListeners.add(listener)
    }

    fun callAction() {
        action(this)
    }

}