package net.dankito.utils.clipboard


abstract class ClipboardWatcherBase {

    protected abstract fun startListeningToClipboard()

    protected abstract fun stopListeningToClipboard()


    protected val listeners = mutableListOf<(ClipboardContent) -> Unit>()


    open fun addClipboardContentChangedListener(listener: (ClipboardContent) -> Unit) {
        if (listeners.isEmpty()) {
            startListeningToClipboard()
        }

        listeners.add(listener)
    }

    open fun removeClipboardContentChangedListener(listener: (ClipboardContent) -> Unit) {
        listeners.remove(listener)

        if (listeners.isEmpty()) {
            stopListeningToClipboard()
        }
    }


    protected open fun clipboardContentChanged(content: ClipboardContent) {
        ArrayList(listeners).forEach { listener ->
            listener(content)
        }
    }

}