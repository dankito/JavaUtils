package net.dankito.utils.clipboard

import net.dankito.utils.ui.image.Image
import java.io.File


open class ClipboardContent(open val plainText: String? = null,
                            open val url: String? = null,
                            open val html: String? = null,
                            open val rtf: String? = null,
                            open val image: Image? = null,
                            open val files: List<File> = listOf()
) {


    open val hasPlainText: Boolean
        get() = plainText != null


    open val hasUrl: Boolean
        get() = url != null


    open val hasHtml: Boolean
        get() = html != null


    open val hasRtf: Boolean
        get() = rtf != null


    open val hasImage: Boolean
        get() = image != null


    open val hasFiles: Boolean
        get() = files.isNotEmpty()

}
