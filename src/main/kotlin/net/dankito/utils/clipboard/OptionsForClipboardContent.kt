package net.dankito.utils.clipboard


data class OptionsForClipboardContent(val headerTitle: String, val options: List<ClipboardContentOption>)