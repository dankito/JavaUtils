package net.dankito.utils.ui.dialogs


enum class ConfirmationDialogButton {

    Confirm,
    No,
    ThirdButton

}