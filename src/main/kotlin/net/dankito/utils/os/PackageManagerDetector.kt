package net.dankito.utils.os

import net.dankito.utils.process.CommandConfig
import net.dankito.utils.process.CommandExecutor
import net.dankito.utils.process.ICommandExecutor


open class PackageManagerDetector(
    protected val commandExecutor: ICommandExecutor = CommandExecutor()
) {

    /**
     * If these package managers are installed can be check once at start up time as these are basic system programs
     * that don't get installed or uninstalled during application run time.
     */

    open val isDebianAptInstalled = isPackageInstallerInstalled("apt-get", "-v")

    open val isRedHatDnfInstalled = isPackageInstallerInstalled("dnf", "-v")

    open val isSuseZypperInstalled = isPackageInstallerInstalled("zypper", "-v")

    open val isArchLinuxPacmanInstalled = isPackageInstallerInstalled("pacman", "--version")


    /**
     * These package managers may be installed or uninstalled during application run time. Therefore check for their
     * existence on each call to their getter.
     */

    open val isMacOsHomeBrewInstalled: Boolean
        get() = isPackageInstallerInstalled("brew", "-v")

    open val isMacOsMacPortsInstalled: Boolean
        get() = isPackageInstallerInstalled("port", "-v") // TODO: is this command really working


    open fun findOsPackageInstaller(): PackageManager {
        findLinuxPackageInstaller()?.let {  packageInstaller ->
            return packageInstaller
        }

        findMacOsPackageInstaller()?.let { packageInstaller ->
            return packageInstaller
        }

        return PackageManager.Unknown
    }

    open fun findLinuxPackageInstaller(): PackageManager? {
        return when {
            isDebianAptInstalled -> PackageManager.apt
            isRedHatDnfInstalled -> PackageManager.dnf
            isSuseZypperInstalled -> PackageManager.zypper
            isArchLinuxPacmanInstalled -> PackageManager.pacman
            else -> null
        }
    }

    open fun findMacOsPackageInstaller(): PackageManager? {
        return when {
            isMacOsHomeBrewInstalled -> PackageManager.HomeBrew
            isMacOsMacPortsInstalled -> PackageManager.MacPorts
            else -> null
        }
    }

    protected open fun isPackageInstallerInstalled(vararg commandArgs: String): Boolean {
        val config = CommandConfig(commandArgs.toList(), logErrors = false)

        return commandExecutor.executeCommandWithLittleOutput(config).successful
    }

}