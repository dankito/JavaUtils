package net.dankito.utils.os


enum class OsType {

    Android,

    Linux,

    MacOs,

    Windows,

    Unknown

}