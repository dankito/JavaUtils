package net.dankito.utils.os


enum class PackageManager {

    apt,

    dnf,

    zypper,

    pacman,

    HomeBrew,

    MacPorts,

    Unknown

}