package net.dankito.utils.os

import net.dankito.utils.info.SystemProperties
import org.slf4j.LoggerFactory


open class OsHelper {

    companion object {
        private val log = LoggerFactory.getLogger(OsHelper::class.java)
    }


    protected val systemProperties = SystemProperties()


    open val osName = systemProperties.osName

    open val osVersion = systemProperties.osVersion

    open val cpuArchitecture = systemProperties.cpuArchitecture

    open val isRunningOnAndroid = determineIfIsRunningOnAndroid()

    open val osType = determineOsType() // has to be defined after isRunningOnAndroid

    open val isRunningOnLinux = osType == OsType.Linux // has to be defined after osType

    open val isRunningOnMacOs = osType == OsType.MacOs

    open val isRunningOnWindows = osType == OsType.Windows


    open val androidVersion: Int
        get() {
            try {
                val versionClass = Class.forName("android.os.Build\$VERSION")
                val sdkIntField = versionClass.getDeclaredField("SDK_INT")
                return sdkIntField.get(null) as Int
            } catch (e: Exception) {
                log.error("Could not get Android version via reflection", e)
            }

            return 0
        }


    open fun isRunningOnJavaSeOrOnAndroidApiLevelAtLeastOf(minimumApiLevel: Int): Boolean {
        return isRunningOnAndroid == false || isRunningOnAndroidAtLeastOfApiLevel(minimumApiLevel)
    }

    open fun isRunningOnAndroidApiLevel(apiLevel: Int): Boolean {
        return isRunningOnAndroid && androidVersion == apiLevel
    }

    open fun isRunningOnAndroidAtLeastOfApiLevel(minimumApiLevel: Int): Boolean {
        return isRunningOnAndroid && androidVersion >= minimumApiLevel
    }


    protected open fun determineIfIsRunningOnAndroid(): Boolean {
        try {
            Class.forName("android.app.Activity")
            return true
        } catch (ex: Exception) { }

        return false
    }

    protected open fun determineOsType(): OsType {
        val lowerCaseOsName = osName.toLowerCase()

        return when {
            isRunningOnAndroid -> OsType.Android // check before Linux as "os.name" is also set to 'Linux' on Android
            lowerCaseOsName.contains("linux") -> OsType.Linux
            lowerCaseOsName.contains("macos") -> OsType.MacOs // TODO: check which value is returned on MacOs
            lowerCaseOsName.contains("windows") -> OsType.Windows
            else -> OsType.Unknown
        }
    }

}
