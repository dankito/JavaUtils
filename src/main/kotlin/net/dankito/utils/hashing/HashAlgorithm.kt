package net.dankito.utils.hashing


enum class HashAlgorithm constructor(val algorithmName: String) {

    MD5("MD5"),
    SHA1("SHA1"),
    /**
     * Be aware older Androids don't support SHA-224 (but SHA-256 and SHA-512).
     */
    SHA224("SHA-224"),
    SHA256("SHA-256"),
    SHA384("SHA-384"),
    SHA512("SHA-512");


    override fun toString(): String {
        return algorithmName
    }

}
