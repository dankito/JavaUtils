package net.dankito.utils.stream

import java.io.OutputStream


open class StringOutputStream : OutputStream() {

    protected val builder = StringBuilder()


    open val string: String
        get() = builder.toString()


    override fun write(byte: Int) {
        builder.append(byte.toChar())
    }

}