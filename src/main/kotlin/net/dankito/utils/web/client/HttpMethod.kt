package net.dankito.utils.web.client


enum class HttpMethod {

    GET,
    POST

}