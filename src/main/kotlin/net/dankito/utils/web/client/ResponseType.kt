package net.dankito.utils.web.client


enum class ResponseType {

    String,
    Bytes,
    Stream

}