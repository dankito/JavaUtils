package net.dankito.utils.web.client


open class RequestParameters(val url: String, var body: String? = null,
                             var contentType: String? = null,
                             var userAgent: String = DefaultUserAgent,
                             var headers: Map<String, String> = mutableMapOf(),
                             var cookies: List<Cookie> = mutableListOf(),
                            // TODO: re-enable setting connection timeout
//                             var connectionTimeoutMillis: Int = RequestParameters.DefaultConnectionTimeoutMillis,
                             var countConnectionRetries: Int = DefaultCountConnectionRetries,
                             var responseType: ResponseType = ResponseType.String,
                             var downloadBufferSize: Int = DefaultDownloadBufferSize,
                             var downloadProgressListener: ((progress: Float, downloadedChunk: ByteArray) -> Unit)? = null) {

    companion object {
        const val DefaultUserAgent = "Mozilla/5.0 (Windows NT 6.3; rv:55.0) Gecko/20100101 Firefox/55.0"

        const val DefaultMobileUserAgent = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/537.36 (KHTML, like Gecko) CChrome/60.0.3112.105 Safari/537.36"

        const val DefaultConnectionTimeoutMillis = 2000

        const val DefaultReadTimeoutMillis = 15000

        const val DefaultWriteTimeoutMillis = 30000

        const val DefaultDownloadBufferSize = 8 * 1024

        const val DefaultCountConnectionRetries = 2
    }


    open fun isBodySet(): Boolean {
        return body.isNullOrBlank() == false
    }

    open fun isUserAgentSet(): Boolean {
        return userAgent.isNullOrBlank() == false
    }

//    open fun isConnectionTimeoutSet(): Boolean {
//        return connectionTimeoutMillis > 0
//    }

    open fun isCountConnectionRetriesSet(): Boolean {
        return countConnectionRetries > 0
    }

    open fun decrementCountConnectionRetries() {
        this.countConnectionRetries--
    }

}