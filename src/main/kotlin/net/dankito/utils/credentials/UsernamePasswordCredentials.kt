package net.dankito.utils.credentials


class UsernamePasswordCredentials(
        val username: String,
        val password: String)
    : ICredentials {

    override fun toString(): String {
        return username
    }

}