package net.dankito.utils

import java.util.concurrent.Executors


open class ThreadPool : IThreadPool {

    protected val threadPool = Executors.newCachedThreadPool()


    override fun runAsync(runnable: () -> Unit) {
        threadPool.execute(runnable)
    }

    override fun shutDown() {
        if (threadPool.isShutdown == false) {
            threadPool.shutdownNow()
        }
    }

}