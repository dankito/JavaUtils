package net.dankito.utils.html

import org.jsoup.nodes.Node
import org.jsoup.select.NodeTraversor


fun Node.toPlainText(): String {
    val formatter = FormattingVisitor()
    NodeTraversor.traverse(formatter, this) // walk the DOM, and call .head() and .tail() for each node

    return formatter.toString()
}