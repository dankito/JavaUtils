package net.dankito.utils.image


enum class ImageOrientation {

    Normal,
    RotatedBy90Deg,
    RotatedBy180Deg,
    RotatedBy270Deg,
    FlippedHorizontally,
    FlippedVertically

}