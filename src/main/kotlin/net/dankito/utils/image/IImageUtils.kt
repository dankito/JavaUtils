package net.dankito.utils.image

import java.io.File
import java.io.IOException


interface IImageUtils {

    @Throws(IOException::class)
    fun getImageOrientationInDegree(imagePath: String): Int

    @Throws(IOException::class)
    fun getImageOrientationInDegree(imagePath: File): Int

    @Throws(IOException::class)
    fun getImageOrientation(imagePath: String): ImageOrientation

    @Throws(IOException::class)
    fun getImageOrientation(imagePath: File): ImageOrientation

}