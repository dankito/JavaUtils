package net.dankito.utils.io

import java.io.File


class FileFileInfoWrapper(val fileInfo: FileInfo) : File(fileInfo.absolutePath) {

    override fun getName(): String {
        return fileInfo.name
    }

    override fun getAbsolutePath(): String {
        return fileInfo.absolutePath
    }

    override fun isDirectory(): Boolean {
        return fileInfo.isDirectory
    }

    override fun isFile(): Boolean {
        return !!! fileInfo.isDirectory
    }

    override fun length(): Long {
        return fileInfo.size
    }

    override fun getParentFile(): File? {
        parent?.takeIf { !!! it.isNullOrBlank() }?.let { parentPath ->
            val parentFile = File(parentPath)
            return FileFileInfoWrapper(FileInfo(parentFile.name, parentPath, true, 0))
        }

        return super.getParentFile()
    }

}