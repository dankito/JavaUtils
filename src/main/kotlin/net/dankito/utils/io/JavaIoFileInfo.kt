package net.dankito.utils.io

import java.io.File


open class JavaIoFileInfo(@Transient val file: File) : FileInfo() {

    override val name: String
        get() = if (file.name.isEmpty()) file.path else file.name // for root file.name is empty -> show '/'

    override val absolutePath: String
        get() = file.absolutePath

    override val isDirectory: Boolean
        get() = file.isDirectory

    override val size: Long
        get() = file.length()

    override val parent: FileInfo?
        get() = file.parentFile?.let { JavaIoFileInfo(it) }

    override fun toFile(): File {
        return file
    }

    override var subFiles: List<FileInfo>?
        get() {
            return super.subFiles ?:
                    FileUtils().getFilesOfDirectorySortedAsFileInfo(file)
        }
        set(value) { super.subFiles = value }

}