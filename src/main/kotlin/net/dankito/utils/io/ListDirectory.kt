package net.dankito.utils.io


enum class ListDirectory {

    DirectoriesAndFiles,
    DirectoriesOnly,
    FilesOnly

}