package net.dankito.utils.io

import net.dankito.utils.extensions.toFileInfo
import java.io.File


open class FileInfo(open val name: String, open val absolutePath: String, open val isDirectory: Boolean,
                    open val size: Long, open val id: String = absolutePath,
                    open var subFiles: List<FileInfo>? = null) {

    companion object {

        @JvmOverloads
        fun from(file: File, name: String = file.name, isDirectory: Boolean = file.isDirectory): FileInfo {
            return FileInfo(name, file.absolutePath, isDirectory, if (isDirectory) 0L else file.length())
        }

    }

    protected constructor() : this("", "", false, 0L)


    open val parent: FileInfo?
        get() = File(absolutePath).parentFile?.toFileInfo()


    open fun toFile(): File {
        return FileFileInfoWrapper(this)
    }


    override fun toString(): String {
        return name
    }

}