package net.dankito.utils.process


class CpuInfo {

    companion object {

        val CountCores: Int = Runtime.getRuntime().availableProcessors()

    }

}