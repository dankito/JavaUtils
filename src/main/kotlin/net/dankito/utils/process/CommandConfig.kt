package net.dankito.utils.process

import java.io.File


open class CommandConfig @JvmOverloads constructor(
    val commandArgs: List<String>,
    val workingDir: File? = null,
    val environmentVariables: Map<String, String> = mapOf(),
    val logErrors: Boolean = true
) {

    override fun toString(): String {
        return commandArgs.joinToString(" ")
    }

}