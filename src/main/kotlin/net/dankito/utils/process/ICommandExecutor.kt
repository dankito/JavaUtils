package net.dankito.utils.process

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope


interface ICommandExecutor {

    /**
     * Do not call this for commands that have a large standard or error output!
     *
     * This method does not read process' standard and error output on extra threads like [executeCommand] does.
     *
     * Standard and error output are being read on same thread after process ended. Therefore if standard and error
     * InputStream reader's buffer is not large enough, the call to Process.waitFor() will hang forever.
     *
     * So it has slightly better performance than [executeCommand] has as it doesn't create two new threads but at the
     * cost that app may hangs forever.
     */
    fun executeCommandWithLittleOutput(vararg arguments: String): ExecuteCommandResult

    /**
     * Do not call this for commands that have a large standard or error output!
     *
     * This method does not read process' standard and error output on extra threads like [executeCommand] does.
     *
     * Standard and error output are being read on same thread after process ended. Therefore if standard and error
     * InputStream reader's buffer is not large enough, the call to Process.waitFor() will hang forever.
     *
     * So it has slightly better performance than [executeCommand] has as it doesn't create two new threads but at the
     * cost that app may hangs forever.
     */
    fun executeCommandWithLittleOutput(config: CommandConfig): ExecuteCommandResult


    fun executeCommand(vararg arguments: String): ExecuteCommandResult

    fun executeCommand(config: CommandConfig): ExecuteCommandResult

    suspend fun executeCommandSuspendable(vararg arguments: String, scope: CoroutineScope): ExecuteCommandResult

    suspend fun executeCommandSuspendable(config: CommandConfig, scope: CoroutineScope = GlobalScope): ExecuteCommandResult

}