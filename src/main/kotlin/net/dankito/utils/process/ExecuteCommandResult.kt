package net.dankito.utils.process


open class ExecuteCommandResult(val exitCode: Int, val outputLines: List<String>, val errorLines: List<String>) {

    val successful: Boolean = exitCode == 0

    val output: String = outputLines.joinToString("\r\n")

    val errors: String = errorLines.joinToString("\r\n")


    override fun toString(): String {
        return "Successful? $successful ($exitCode): $output. Errors: $errors"
    }

}