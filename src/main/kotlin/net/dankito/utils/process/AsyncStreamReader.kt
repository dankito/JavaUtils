package net.dankito.utils.process

import org.slf4j.LoggerFactory
import java.io.InputStream


open class AsyncStreamReader(protected val inputStream: InputStream) : Thread() {

    companion object {
        private val log = LoggerFactory.getLogger(AsyncStreamReader::class.java)
    }


    protected val linesField = mutableListOf<String>()

    val lines: List<String>
        get() {
            this.join() // wait till reading from stream is done

            return linesField
        }


    override fun run() {
        try {
            inputStream.bufferedReader().use { reader ->
                reader.forEachLine {
                    linesField.add(it)
                }
            }
        } catch (e: Exception) {
            log.error("Error occurred while reading stream", e)
        }
    }

}