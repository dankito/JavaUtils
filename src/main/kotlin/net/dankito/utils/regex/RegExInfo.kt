package net.dankito.utils.regex


open class RegExInfo {

    companion object {
        val ReservedRegExCharacters = listOf("\\", "^", "$", ".", "|", "?", "*", "+", "(", ")", "[", "{")
    }

}