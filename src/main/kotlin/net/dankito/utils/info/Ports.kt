package net.dankito.utils.info


class Ports {

    companion object {
        /**
         * Theoretically for UDP this is not true as there 0 is also allowed but means 'no port'.
         */
        const val MinAllowedPortNumber = 1

        const val MaxAllowedPortNumber = 65535

        const val MaxWellKnownPortNumber = 1023


        fun isValidPortNumber(port: Int): Boolean {
            return port >= MinAllowedPortNumber && port <= MaxAllowedPortNumber
        }

        fun isValidWellKnownPortNumber(port: Int): Boolean {
            return port >= MinAllowedPortNumber && port <= MaxWellKnownPortNumber
        }

        fun isValidNotWellKnownPortNumber(port: Int): Boolean {
            return port > MaxWellKnownPortNumber && port <= MaxAllowedPortNumber
        }
    }

}