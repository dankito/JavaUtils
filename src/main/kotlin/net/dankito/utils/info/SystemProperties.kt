package net.dankito.utils.info

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File


open class SystemProperties {

    companion object {
        const val UserNamePropertyName = "user.name"

        const val UserCountryPropertyName = "user.country"

        const val UserLanguagePropertyName = "user.language"

        const val UserHomeDirectoryPropertyName = "user.home"

        const val OsNamePropertyName = "os.name"

        const val OsVersionPropertyName = "os.version"

        const val CpuArchitecturePropertyName = "os.arch"

        const val JavaVersionPropertyName = "java.specification.version"

        const val JavaRuntimeVersionPropertyName = "java.runtime.version"

        const val StartupDirectoryPropertyName = "user.dir"

        const val StartupClassPathPropertyName = "java.class.path"


        private val log = LoggerFactory.getLogger(SystemProperties::class.java)
    }


    open val propertyNames: Set<String> = System.getProperties().stringPropertyNames()

    open val propertyValues: Map<String, String> = propertyNames.associateWith { System.getProperty(it) }


    open val userName: String = getProperty(UserNamePropertyName, "")

    open val userCountry: String = getProperty(UserCountryPropertyName, "")

    open val userLanguage: String = getProperty(UserLanguagePropertyName, "")

    open val userHomeDirectory: String = getProperty(UserHomeDirectoryPropertyName, "")

    open val osName: String = getProperty(OsNamePropertyName, "")

    open val osVersion: String = getProperty(OsVersionPropertyName, "")

    open val cpuArchitecture: String = getProperty(CpuArchitecturePropertyName, "")

    open val javaVersion: String = getProperty(JavaVersionPropertyName, "")

    open val javaRuntimeVersion: String = getProperty(JavaRuntimeVersionPropertyName, "")

    open val startupDirectory: String = getProperty(StartupDirectoryPropertyName, "")

    open val startupClassPath: String = getProperty(StartupClassPathPropertyName, "")


    open fun getProperty(name: String): String? {
        return propertyValues[name]
    }

    open fun getProperty(name: String, defaultValue: String): String {
        return getProperty(name) ?: defaultValue
    }


    open fun printAllProperties() {
        doForEachProperty { name, value ->
            println("$name: $value")
        }
    }

    open fun logAllProperties(log: Logger) {
        doForEachProperty { name, value ->
            log.info("$name: $value")
        }
    }

    open fun doForEachProperty(property: (name: String, value: String) -> Unit) {
        propertyValues.forEach { entry ->
            property(entry.key, entry.value)
        }
    }


    /**
     * Due to a bug in some JVM Linux implementations user.dir does not point to start-up directory - that is the directory current .jar is started from - but to user.home.
     *
     * Or on some servers start-up directory points to "/" and therefore being write protected.
     *
     * This method fixes this.
     */
    @JvmOverloads
    open fun fixStartupDirectory(classInJarInStartupDirectory: Class<*> = javaClass): File {
        try {
            val systemProperties = SystemProperties()

            if (systemProperties.startupDirectory == systemProperties.userHomeDirectory ||
                systemProperties.startupDirectory == "/") {
                val startUpDir = File(classInJarInStartupDirectory.protectionDomain.codeSource.location.toURI()).parentFile

                System.getProperties().setProperty(StartupDirectoryPropertyName, startUpDir.absolutePath)

                return startUpDir
            }

            return File(systemProperties.startupDirectory)
        } catch (e: Exception) {
            // don't create Logger and therefore log file when SystemProperty class gets created as at this point current working directory is not fixed yet
            log.error("Could not set fix working dir", e)
        }

        return File(System.getProperty(StartupDirectoryPropertyName))
    }

}