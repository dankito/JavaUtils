package net.dankito.utils.events


interface IEventBus {

    fun post(event: Any)

    fun <T> subscribe(eventClass: Class<T>, listener: (T) -> Unit): ISubscribedEvent

}