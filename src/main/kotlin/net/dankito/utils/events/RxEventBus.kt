package net.dankito.utils.events

import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject


class RxEventBus : IEventBus, IRxEventBus {

    private val bus = PublishSubject.create<Any>()


    override fun post(event: Any) {
        bus.onNext(event)
    }


    override fun <T> subscribe(eventClass: Class<T>, listener: (T) -> Unit): ISubscribedEvent {
        val subscribedFlowable = subscribe(eventClass)

        val disposable = subscribedFlowable.subscribe { listener(it) }

        return SubscribedRxEvent(disposable)
    }

    override fun <T> subscribe(eventClass: Class<T>): Flowable<T> {
        return bus
                .toFlowable(BackpressureStrategy.BUFFER)
                .filter { event -> event.javaClass == eventClass }
                .map<T> { event -> event as T }
    }

}