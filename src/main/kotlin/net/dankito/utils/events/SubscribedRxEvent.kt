package net.dankito.utils.events

import io.reactivex.disposables.Disposable


open class SubscribedRxEvent(protected val disposable: Disposable) : ISubscribedEvent {

    override fun unsubscribe() {
        if (disposable.isDisposed == false) {
            disposable.dispose()
        }
    }

}