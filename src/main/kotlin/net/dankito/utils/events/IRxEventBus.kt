package net.dankito.utils.events

import io.reactivex.Flowable


interface IRxEventBus : IEventBus {

    fun <T> subscribe(eventClass: Class<T>): Flowable<T>

}