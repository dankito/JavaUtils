package net.dankito.utils.events


interface ISubscribedEvent {

    fun unsubscribe()

}