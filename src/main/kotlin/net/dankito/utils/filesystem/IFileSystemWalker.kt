package net.dankito.utils.filesystem

import java.nio.file.FileVisitResult
import java.nio.file.Path


interface IFileSystemWalker {

    fun listFiles(startDir: Path): List<Path>

    fun listFilesAndFolders(startDir: Path): FileSystemWalkResult


    fun walk(startDir: Path, discoveredFileCallback: (Path) -> Unit)

    fun listAllFilesAndFoldersWalk(startDir: Path, preVisitDirectory: ((directory: VisitedFile) -> Unit)?,
                                   visitedFileCallback: (VisitedFile) -> Unit)

    fun detailedWalk(startDir: Path, abortOnError: Boolean = false,
                     preVisitDirectory: ((directory: VisitedFile) -> FileVisitResult?)? = null,
                     postVisitDirectory: ((directory: VisitedFile) -> FileVisitResult?)? = null,
                     visitedFileCallback: (VisitedFile) -> FileVisitResult?)

}