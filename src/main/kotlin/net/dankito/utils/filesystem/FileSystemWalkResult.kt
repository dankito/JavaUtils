package net.dankito.utils.filesystem


class FileSystemWalkResult(
        val discoveredFiles: List<VisitedFile>,
        val discoveredFolders: List<VisitedFile>
) {

    override fun toString(): String {
        return "${discoveredFiles.size} files and ${discoveredFolders.size} folders"
    }

}