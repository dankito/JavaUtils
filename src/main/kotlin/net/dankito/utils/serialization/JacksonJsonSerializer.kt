package net.dankito.utils.serialization

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import net.dankito.utils.io.FileUtils
import java.io.File


open class JacksonJsonSerializer(protected val fileUtils: FileUtils = FileUtils(),
                                 configureMapperCallback: ((mapper: ObjectMapper) -> Unit)? = null) : ISerializer {

    protected val objectMapper = ObjectMapper()


    constructor(configureMapperCallback: ((mapper: ObjectMapper) -> Unit)? = null)
            : this(FileUtils(), configureMapperCallback)


    init {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        // only serialize fields
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE)
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)

        configureMapperCallback?.invoke(objectMapper)
    }


    override val serializationFormat = ISerializer.SerializationFormatJson


    override fun serializeObject(obj: Any, outputFile: File) {
        val json = serializeObject(obj)
        fileUtils.writeToTextFile(json, outputFile)
    }

    override fun serializeObject(obj: Any): String {
        return objectMapper.writeValueAsString(obj)
    }


    override fun <T> deserializeObject(serializedObjectFile: File, objectClass: Class<T>, vararg genericParameterTypes: Class<*>): T? {
        if (serializedObjectFile.exists()) {
            fileUtils.readFromTextFile(serializedObjectFile)?.let { json ->
                return deserializeObject(json, objectClass, *genericParameterTypes)
            }
        }

        return null
    }


    override fun <T> deserializeList(serializedObjectFile: File, genericListParameterType: Class<T>): List<T>? {
        return deserializeObject(serializedObjectFile, List::class.java, genericListParameterType) as? List<T>
    }

    override fun <T> deserializeListOr(serializedObjectFile: File, genericListParameterType: Class<T>,
                                       defaultValue: List<T>): List<T> {
        return deserializeList(serializedObjectFile, genericListParameterType) ?: defaultValue
    }


    override fun <T> deserializeSet(serializedObjectFile: File, genericSetParameterType: Class<T>): Set<T>? {
        return deserializeObject(serializedObjectFile, Set::class.java, genericSetParameterType) as? Set<T>
    }

    override fun <T> deserializeSetOr(serializedObjectFile: File, genericSetParameterType: Class<T>,
                                      defaultValue: Set<T>): Set<T> {

        return deserializeSet(serializedObjectFile, genericSetParameterType) ?: defaultValue
    }


    override fun <Key, Value> deserializeMap(serializedObjectFile: File, keyParameterType: Class<Key>,
                                             valueParameterType: Class<Value>): Map<Key, Value>? {

        return deserializeObject(serializedObjectFile, Map::class.java, keyParameterType, valueParameterType) as? Map<Key, Value>
    }

    override fun <Key, Value> deserializeMapOr(serializedObjectFile: File, keyParameterType: Class<Key>,
                                   valueParameterType: Class<Value>, defaultValue: Map<Key, Value>): Map<Key, Value> {

        return deserializeMap(serializedObjectFile, keyParameterType, valueParameterType) ?: defaultValue
    }



    override fun <T> deserializeObject(serializedObject: String, objectClass: Class<T>, vararg genericParameterTypes: Class<*>): T {
        if(genericParameterTypes.isEmpty()) {
            return objectMapper.readValue(serializedObject, objectClass)
        }
        else {
            return objectMapper.readValue<T>(serializedObject, objectMapper.typeFactory.constructParametricType(objectClass, *genericParameterTypes))
        }
    }


    override fun <T> deserializeList(serializedObject: String, genericListParameterType: Class<T>): List<T>? {
        return deserializeObject(serializedObject, List::class.java, genericListParameterType) as? List<T>
    }

    override fun <T> deserializeListOr(serializedObject: String, genericListParameterType: Class<T>,
                                       defaultValue: List<T>): List<T> {
        return deserializeList(serializedObject, genericListParameterType) ?: defaultValue
    }


    override fun <T> deserializeSet(serializedObject: String, genericSetParameterType: Class<T>): Set<T>? {
        return deserializeObject(serializedObject, Set::class.java, genericSetParameterType) as? Set<T>
    }

    override fun <T> deserializeSetOr(serializedObject: String, genericSetParameterType: Class<T>,
                                      defaultValue: Set<T>): Set<T> {

        return deserializeSet(serializedObject, genericSetParameterType) ?: defaultValue
    }


    override fun <Key, Value> deserializeMap(serializedObject: String, keyParameterType: Class<Key>,
                                             valueParameterType: Class<Value>): Map<Key, Value>? {

        return deserializeObject(serializedObject, Map::class.java, keyParameterType, valueParameterType) as? Map<Key, Value>
    }

    override fun <Key, Value> deserializeMapOr(serializedObject: String, keyParameterType: Class<Key>,
                                   valueParameterType: Class<Value>, defaultValue: Map<Key, Value>): Map<Key, Value> {

        return deserializeMap(serializedObject, keyParameterType, valueParameterType) ?: defaultValue
    }

}