package net.dankito.utils.serialization

import java.io.File


interface ISerializer {

    companion object {
        const val SerializationFormatJson = "JSON"
        const val SerializationFormatXml = "XML"
    }


    val serializationFormat: String


    fun serializeObject(obj: Any, outputFile: File)

    fun serializeObject(obj: Any) : String


    fun <T> deserializeObject(serializedObjectFile: File, objectClass: Class<T>, vararg genericParameterTypes: Class<*>): T?

    fun <T> deserializeList(serializedObjectFile: File, genericListParameterType: Class<T>) : List<T>?
    fun <T> deserializeListOr(serializedObjectFile: File, genericListParameterType: Class<T>,
                              defaultValue: List<T> = listOf()) : List<T>

    fun <T> deserializeSet(serializedObjectFile: File, genericSetParameterType: Class<T>) : Set<T>?
    fun <T> deserializeSetOr(serializedObjectFile: File, genericSetParameterType: Class<T>,
                             defaultValue: Set<T> = setOf()) : Set<T>

    fun <Key, Value> deserializeMap(serializedObjectFile: File, keyParameterType: Class<Key>,
                                    valueParameterType: Class<Value>) : Map<Key, Value>?
    fun <Key, Value> deserializeMapOr(serializedObjectFile: File, keyParameterType: Class<Key>,
                          valueParameterType: Class<Value>, defaultValue: Map<Key, Value> = mapOf()) : Map<Key, Value>


    fun <T> deserializeObject(serializedObject: String, objectClass: Class<T>, vararg genericParameterTypes: Class<*>) : T

    fun <T> deserializeList(serializedObject: String, genericListParameterType: Class<T>) : List<T>?
    fun <T> deserializeListOr(serializedObject: String, genericListParameterType: Class<T>,
                              defaultValue: List<T> = listOf()) : List<T>?

    fun <T> deserializeSet(serializedObject: String, genericSetParameterType: Class<T>) : Set<T>?
    fun <T> deserializeSetOr(serializedObject: String, genericSetParameterType: Class<T>,
                             defaultValue: Set<T> = setOf()) : Set<T>?

    fun <Key, Value> deserializeMap(serializedObject: String, keyParameterType: Class<Key>,
                                    valueParameterType: Class<Value>) : Map<Key, Value>?
    fun <Key, Value> deserializeMapOr(serializedObject: String, keyParameterType: Class<Key>,
                            valueParameterType: Class<Value>, defaultValue: Map<Key, Value> = mapOf()) : Map<Key, Value>


    fun isSerializingToJson(): Boolean {
        return serializationFormat.equals(SerializationFormatJson, true)
    }

    fun isSerializingToXml(): Boolean {
        return serializationFormat.equals(SerializationFormatXml, true)
    }

}