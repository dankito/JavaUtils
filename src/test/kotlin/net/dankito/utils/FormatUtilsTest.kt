package net.dankito.utils

import net.dankito.utils.FormatUtils.Companion.GigaBits
import net.dankito.utils.FormatUtils.Companion.GigaBytePowerOfTwo
import net.dankito.utils.FormatUtils.Companion.KiloBits
import net.dankito.utils.FormatUtils.Companion.KiloBytePowerOfTwo
import net.dankito.utils.FormatUtils.Companion.MegaBits
import net.dankito.utils.FormatUtils.Companion.MegaBytePowerOfTwo
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


class FormatUtilsTest {

    private val underTest = FormatUtils()


    @Test
    fun formatFileSize_Long_3_2GB() {
        val result = underTest.formatFileSize((3.2 * GigaBytePowerOfTwo).toLong())

        assertThat(result).isEqualTo("${formatSize(3.2f)} GB")
    }

    @Test
    fun formatFileSize_Long_0_1GB() {
        val result = underTest.formatFileSize((0.1 * GigaBytePowerOfTwo).toLong())

        assertThat(result).isEqualTo("${formatSize(0.1f)} GB")
    }

    @Test
    fun formatFileSize_Long_87_6MB() {
        val result = underTest.formatFileSize((87.6 * MegaBytePowerOfTwo).toLong())

        assertThat(result).isEqualTo("${formatSize(87.6f)} MB")
    }

    @Test
    fun formatFileSize_Long_1_2MB() {
        val result = underTest.formatFileSize((1.2 * MegaBytePowerOfTwo).toLong())

        assertThat(result).isEqualTo("${formatSize(1.2f)} MB")
    }

    @Test
    fun formatFileSize_Long_100KB() {
        val result = underTest.formatFileSize((100 * KiloBytePowerOfTwo).toLong())

        assertThat(result).isEqualTo("${formatSize(100.0f)} kB")
    }

    @Test
    fun formatFileSize_Long_100Bytes() {
        val result = underTest.formatFileSize(100L)

        assertThat(result).isEqualTo("${formatSize(0.1f)} kB")
    }

    @Test
    fun formatFileSize_Long_10Bytes() {
        val result = underTest.formatFileSize(10L)

        assertThat(result).isEqualTo("${formatSize(0.0f)} kB")
    }



    @Test
    fun formatFileSpeed_Long_3_2GBitsPerSecond() {
        val result = underTest.formatSpeed((3.2 * GigaBits).toLong())

        assertThat(result).isEqualTo("${formatSize(3.2f)} GBit/s")
    }

    @Test
    fun formatFileSpeed_Long_0_1GBitsPerSecond() {
        val result = underTest.formatSpeed((GigaBits / 10).toLong())

        assertThat(result).isEqualTo("${formatSize(0.1f)} GBit/s")
    }

    @Test
    fun formatFileSpeed_Long_87_6MBitsPerSecond() {
        val result = underTest.formatSpeed((87.6 * MegaBits).toLong())

        assertThat(result).isEqualTo("${formatSize(87.6f)} MBit/s")
    }

    @Test
    fun formatFileSpeed_Long_1_2MBitsPerSecond() {
        val result = underTest.formatSpeed((1.2 * MegaBits).toLong())

        assertThat(result).isEqualTo("${formatSize(1.2f)} MBit/s")
    }

    @Test
    fun formatFileSpeed_Long_100KBitsPerSecond() {
        val result = underTest.formatSpeed((100 * KiloBits).toLong())

        assertThat(result).isEqualTo("${formatSize(0.1f)} MBit/s")
    }

    @Test
    fun formatFileSpeed_Long_100BitsPerSecond() {
        val result = underTest.formatSpeed(100L)

        assertThat(result).isEqualTo("${formatSize(0.1f)} kBit/s")
    }

    @Test
    fun formatFileSpeed_Long_10BitsPerSecond() {
        val result = underTest.formatSpeed(10L)

        assertThat(result).isEqualTo("${formatSize(0.0f)} kBit/s")
    }


    private fun formatSize(size: Float): String {
        return String.format("%.1f", size)
    }
}