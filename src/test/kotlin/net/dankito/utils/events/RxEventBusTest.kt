package net.dankito.utils.events

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.util.concurrent.atomic.AtomicReference

class RxEventBusTest {

    companion object {

        private const val TestData = "TestData"

    }


    private val underTest = RxEventBus()


    @Test
    fun subscribeToPostedEvent() {
        val result = AtomicReference<TestEvent1>(null)

        underTest.subscribe(TestEvent1::class.java) {
            result.set(it)
        }


        // when
        underTest.post(TestEvent1(TestData))


        // then
        assertThat(result.get()).isNotNull
        assertThat(result.get().testData).isEqualTo(TestData)
    }

    @Test
    fun subscribeToNotPostedEvent() {
        val result = AtomicReference<TestEvent1>(null)

        underTest.subscribe(TestEvent1::class.java) {
            result.set(it)
        }


        // when
        underTest.post(TestEvent2())


        // then
        assertThat(result.get()).isNull()
    }


    class TestEvent1(val testData: String)

    class TestEvent2
}