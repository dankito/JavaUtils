package net.dankito.utils.io

import net.dankito.utils.info.SystemProperties
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.io.File

class FileUtilsTest {

    private val homeDirectory = File(SystemProperties().userHomeDirectory)

    private val underTest = FileUtils()


    @Test
    fun getFilesOfDirectory_TestListDirectory() {
        val filesAndFolders = underTest.getFilesOfDirectory(homeDirectory)

        val filesOnly = underTest.getFilesOfDirectory(homeDirectory, ListDirectory.FilesOnly)

        val foldersOnly = underTest.getFilesOfDirectory(homeDirectory, ListDirectory.DirectoriesOnly)

        val filesAndFoldersSize = filesAndFolders?.size ?: 0
        val filesOnlySize = filesOnly?.size ?: 0
        val foldersOnlySize = foldersOnly?.size ?: 0

        assertThat(filesAndFoldersSize).isNotZero()
        assertThat(filesAndFoldersSize).isEqualTo(filesOnlySize + foldersOnlySize)
    }

    @Test
    fun getFilesOfDirectory_TestExtensionsFilter() {
        val files = underTest.getFilesOfDirectory(homeDirectory, ListDirectory.FilesOnly)
        val extensions = files?.groupingBy { it.extension.toLowerCase() }?.eachCount()

        assertThat(extensions).isNotEmpty

        extensions?.forEach { extension, count ->
            val filesWithThatExtensions = underTest.getFilesOfDirectory(homeDirectory, ListDirectory.FilesOnly, 1,
                    listOf(extension))

            assertThat(filesWithThatExtensions).hasSize(count)
            assertThat(filesWithThatExtensions?.map { it.extension.toLowerCase() }?.toSet()).hasSize(1)
        }
    }

    @Test
    fun getFilesOfDirectory_TestFolderDepth() {
        val depth1Files = underTest.getFilesOfDirectory(homeDirectory, folderDepth = 1)

        val depth2Files = underTest.getFilesOfDirectory(homeDirectory, folderDepth = 2)

        val depth3Files = underTest.getFilesOfDirectory(homeDirectory, folderDepth = 3)


        assertThat(depth2Files?.size ?: 0).isGreaterThan(depth1Files?.size ?: 0)

        assertThat(depth3Files?.size ?: 0).isGreaterThan(depth2Files?.size ?: 0)
    }

}