package net.dankito.utils.resources

import net.dankito.utils.io.FileUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.io.File

class ResourceFilesExtractorTest {

    companion object {
        private const val TestFilesSubFolderName = "lib"
        private val TestFilesSubFolder = File(TestFilesSubFolderName)

        private const val TestFile1 = "ResourceExtractorTest1.txt" // this file is in resources root
        private const val TestFile2 = "ResourceExtractorTest2.txt" // and these two are in TestFilesSubFolder
        private const val TestFile3 = "ResourceExtractorTest3.txt"
    }


    private val underTest = ResourceFilesExtractor()


    @Test
    fun extractFile() {

        // given
        val tempDir = getTempDirectory()

        // when
        underTest.extract(listOf(TestFile1), tempDir)

        // then
        val extractedFile = File(tempDir, TestFile1)
        assertThat(extractedFile.exists()).isTrue()

        extractedFile.delete()
    }

    @Test
    fun extractFilesFromSubFolder() {

        // given
        val fileInSubFolder1 = File(TestFilesSubFolder, TestFile2)
        val fileInSubFolder2 = File(TestFilesSubFolder, TestFile3)
        val tempDir = getTempDirectory()

        // when
        underTest.extract(listOf(fileInSubFolder1.path, fileInSubFolder2.path), tempDir)

        // then
        val extractedSubFolder = File(tempDir, TestFilesSubFolderName)
        assertThat(extractedSubFolder.exists()).isTrue()

        val extractedFile1 = File(extractedSubFolder, TestFile2)
        assertThat(extractedFile1.exists()).isTrue()

        val extractedFile2 = File(extractedSubFolder, TestFile3)
        assertThat(extractedFile2.exists()).isTrue()

        FileUtils().deleteFolderRecursively(extractedSubFolder)
    }


    @Test
    fun extractAllFilesFromJar() {

        // given
        val tempDir = getTempDirectory()

        // when
        underTest.extractAllFilesFromJar(ResourceFilesExtractorTest::class.java, TestFilesSubFolder, tempDir)

        // then
        val extractedSubFolder = File(tempDir, TestFilesSubFolderName)
        assertThat(extractedSubFolder.exists()).isTrue()

        val extractedFile1 = File(extractedSubFolder, TestFile2)
        assertThat(extractedFile1.exists()).isTrue()

        val extractedFile2 = File(extractedSubFolder, TestFile3)
        assertThat(extractedFile2.exists()).isTrue()

        FileUtils().deleteFolderRecursively(extractedSubFolder)
    }


    private fun getTempDirectory(): File {
        val tempFile = File.createTempFile("test", "test")
        tempFile.deleteOnExit()

        return tempFile.parentFile
    }

}