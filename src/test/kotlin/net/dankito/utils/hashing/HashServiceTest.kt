package net.dankito.utils.hashing

import net.dankito.utils.Stopwatch
import net.dankito.utils.io.FileUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.slf4j.LoggerFactory
import java.io.File


/**
 * Just a smoke test to see if hashing files generally works and to compare speed of hash algorithms
 */
class HashServiceTest {

    companion object {
        private const val StringToHash = "You must be the change you wish to see in the world."

        private const val StringMd5Hash = "cb801234b517fbd97a5c541a8785be18"

        private const val StringSha1Hash = "85c1605820e4bcdbc26888974d216297accd5a4f"

        private const val StringSha256Hash = "d5ae739cc7166365e9b4c6bd5e750dd385e962c0b9b253968a4886134771571b"

        private const val StringSha512Hash = "135718dfed3ff9ba45de87caa5bcfd93b34f5c6ba0b415896a85ae2da4759fc8d73aef86530ca75e5606fc18afa4c79a8b3870bb256c30e5c4c263d867046d92"


        private val log = LoggerFactory.getLogger(HashServiceTest::class.java)

        private val fixedContentTestFile = createFixedContentTestFile(StringToHash)

        private val largeTestFile = Stopwatch.logDuration("Creating test file", log) { createLargeRandomContentTestFile() }

        private fun createFixedContentTestFile(content: String): File {
            val testFile = File.createTempFile("FileHashTest", ".tmp")

            testFile.bufferedWriter().use { writer ->
                writer.write(content)
            }

            testFile.deleteOnExit()

            return testFile
        }

        private fun createLargeRandomContentTestFile(fileSize: Long = 2 * 1024 * 1024 * 1024L): File {
            val testFile = File.createTempFile("FileHashTest", ".tmp")

            FileUtils().writeRandomContentToFile(testFile, fileSize)

            testFile.deleteOnExit()

            return testFile
        }
    }


    private val underTest = HashService()


    @Test
    fun hashString_MD5() {

        // when
        val result = underTest.hashString(HashAlgorithm.MD5, StringToHash)

        // then
        assertThat(result).isEqualTo(StringMd5Hash)
    }

    @Test
    fun hashString_SHA1() {

        // when
        val result = underTest.hashString(HashAlgorithm.SHA1, StringToHash)

        // then
        assertThat(result).isEqualTo(StringSha1Hash)
    }

    @Test
    fun hashString_SHA256() {

        // when
        val result = underTest.hashString(HashAlgorithm.SHA256, StringToHash)

        // then
        assertThat(result).isEqualTo(StringSha256Hash)
    }

    @Test
    fun hashString_SHA512() {

        // when
        val result = underTest.hashString(HashAlgorithm.SHA512, StringToHash)

        // then
        assertThat(result).isEqualTo(StringSha512Hash)
    }


    @Test
    fun getFileHash_MD5() {
        testFileHash(HashAlgorithm.MD5, StringMd5Hash)
    }

    @Test
    fun getFileHash_Sha1() {
        testFileHash(HashAlgorithm.SHA1, StringSha1Hash)
    }

    @Test
    fun getFileHash_Sha256() {
        testFileHash(HashAlgorithm.SHA256, StringSha256Hash)
    }

    @Test
    fun getFileHash_Sha512() {
        testFileHash(HashAlgorithm.SHA512, StringSha512Hash)
    }


    @Test
    fun fileHashDuration_MD5() {
        testLargeRandomContentFileHash(HashAlgorithm.MD5)
    }

    @Test
    fun fileHashDuration_Sha1() {
        testLargeRandomContentFileHash(HashAlgorithm.SHA1)
    }

    @Test
    fun fileHashDuration_Sha256() {
        testLargeRandomContentFileHash(HashAlgorithm.SHA256)
    }

    @Test
    fun fileHashDuration_Sha512() {
        testLargeRandomContentFileHash(HashAlgorithm.SHA512)
    }


    private fun testFileHash(algorithm: HashAlgorithm, expectedHash: String) {

        // when
        val result = Stopwatch.logDuration("Calculating file hash for small file with $algorithm", log) {
            underTest.getFileHash(algorithm, fixedContentTestFile)
        }


        // then
        log.info("Hash for $algorithm is $result")

        assertThat(result).isEqualTo(expectedHash)
    }

    private fun testLargeRandomContentFileHash(algorithm: HashAlgorithm) {

        // when
        val result = Stopwatch.logDuration("Calculating file hash for large file with $algorithm", log) {
            underTest.getFileHash(algorithm, largeTestFile)
        }


        // then
        log.info("Hash for $algorithm is $result")

        assertThat(result).isNotEmpty()
    }

}