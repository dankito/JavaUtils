package net.dankito.utils

import org.junit.Test
import java.util.concurrent.TimeUnit


class StopwatchTest {

	/*		Just to test log output. You have to check output in console by yourself	*/

	@Test
	fun `Format nanos`() {

		// when
		Stopwatch.logDuration("Stopwatch Test Nanos") {
			TimeUnit.NANOSECONDS.sleep(50)
		}

	}

	@Test
	fun `Format millis`() {

		// when
		Stopwatch.logDuration("Stopwatch Test Millis") {
			TimeUnit.MILLISECONDS.sleep(50)
		}

	}

	@Test
	fun `Format seconds`() {

		// when
		Stopwatch.logDuration("Stopwatch Test Seconds") {
			TimeUnit.SECONDS.sleep(10)
		}

	}

	@Test
	fun `Format minutes`() {

		// when
		Stopwatch.logDuration("Stopwatch Test Minutes") {
			TimeUnit.SECONDS.sleep(70)
		}

	}

}