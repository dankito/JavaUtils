package net.dankito.utils.web

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


class UrlUtilTest {

    private val underTest = UrlUtil()


    @Test
    fun isHttpUri_http_true() {

        // when
        val result = underTest.isHttpUri("http://www.test.com")

        // then
        assertThat(result).isTrue()
    }

    @Test
    fun isHttpUri_https_true() {

        // when
        val result = underTest.isHttpUri("https://www.test.com")

        // then
        assertThat(result).isTrue()
    }

    @Test
    fun isHttpUri_HTTP_true() {

        // when
        val result = underTest.isHttpUri("HTTP://www.test.com")

        // then
        assertThat(result).isTrue()
    }

    @Test
    fun isHttpUri_HTTPS_true() {

        // when
        val result = underTest.isHttpUri("HTTPS://www.test.com")

        // then
        assertThat(result).isTrue()
    }

    @Test
    fun isHttpUri_HtTpS_true() {

        // when
        val result = underTest.isHttpUri("HtTpS://www.test.com")

        // then
        assertThat(result).isTrue()
    }


    @Test
    fun isHttpUri_htt_false() {

        // when
        val result = underTest.isHttpUri("htt://www.test.com")

        // then
        assertThat(result).isFalse()
    }

    @Test
    fun isHttpUri_ttp_false() {

        // when
        val result = underTest.isHttpUri("ttp://www.test.com")

        // then
        assertThat(result).isFalse()
    }


    @Test
    fun extractHttpUri_starts_with_http_uri() {

        // when
        val result = underTest.extractHttpUri("http://www.test.com This is a test text")

        // then
        assertThat(result).isEqualTo("http://www.test.com")
    }

    @Test
    fun extractHttpUri_starts_with_https_uri() {

        // when
        val result = underTest.extractHttpUri("This is a test text https://www.test.com")

        // then
        assertThat(result).isEqualTo("https://www.test.com")
    }

    @Test
    fun extractHttpUri_starts_with_hTTp_uri() {

        // when
        val result = underTest.extractHttpUri("hTTp://www.test.com This is a test text")

        // then
        assertThat(result).isEqualTo("hTTp://www.test.com")
    }

    @Test
    fun extractHttpUri_starts_with_HtTpS_uri() {

        // when
        val result = underTest.extractHttpUri("HtTpS://www.test.com This is a test text")

        // then
        assertThat(result).isEqualTo("HtTpS://www.test.com")
    }


    @Test
    fun extractHttpUri_contains_http_uri() {

        // when
        val result = underTest.extractHttpUri("This is a test text http://www.test.com that contains an uri")

        // then
        assertThat(result).isEqualTo("http://www.test.com")
    }

    @Test
    fun extractHttpUri_contains_https_uri() {

        // when
        val result = underTest.extractHttpUri("This is a test text https://www.test.com that contains an uri")

        // then
        assertThat(result).isEqualTo("https://www.test.com")
    }

    @Test
    fun extractHttpUri_contains_hTTp_uri() {

        // when
        val result = underTest.extractHttpUri("This is a test text hTTp://www.test.com that contains an uri")

        // then
        assertThat(result).isEqualTo("hTTp://www.test.com")
    }

    @Test
    fun extractHttpUri_contains_HtTpS_uri() {

        // when
        val result = underTest.extractHttpUri("This is a test text HtTpS://www.test.com that contains an uri")

        // then
        assertThat(result).isEqualTo("HtTpS://www.test.com")
    }


    @Test
    fun extractHttpUri_ends_with_http_uri() {

        // when
        val result = underTest.extractHttpUri("This is a test text http://www.test.com")

        // then
        assertThat(result).isEqualTo("http://www.test.com")
    }

    @Test
    fun extractHttpUri_ends_with_https_uri() {

        // when
        val result = underTest.extractHttpUri("This is a test text https://www.test.com")

        // then
        assertThat(result).isEqualTo("https://www.test.com")
    }

    @Test
    fun extractHttpUri_ends_with_hTTp_uri() {

        // when
        val result = underTest.extractHttpUri("This is a test text hTTp://www.test.com")

        // then
        assertThat(result).isEqualTo("hTTp://www.test.com")
    }

    @Test
    fun extractHttpUri_ends_with_HtTpS_uri() {

        // when
        val result = underTest.extractHttpUri("This is a test text HtTpS://www.test.com")

        // then
        assertThat(result).isEqualTo("HtTpS://www.test.com")
    }


    @Test
    fun extractHttpUri_http_without_second_slash_null() {

        // when
        val result = underTest.extractHttpUri("This is a test text http:/www.test.com")

        // then
        assertThat(result).isNull()
    }

    @Test
    fun extractHttpUri_http_without_colon_null() {

        // when
        val result = underTest.extractHttpUri("This is a test text http//www.test.com")

        // then
        assertThat(result).isNull()
    }

    @Test
    fun extractHttpUri_htt_null() {

        // when
        val result = underTest.extractHttpUri("This is a test text htt://www.test.com")

        // then
        assertThat(result).isNull()
    }

    @Test
    fun extractHttpUri_ttp_null() {

        // when
        val result = underTest.extractHttpUri("This is a test text ttp://www.test.com")

        // then
        assertThat(result).isNull()
    }

    @Test
    fun `makeLinkAbsolute - Make relative url absolute with siteUrl`() {
        val relativeUrl = "assets/favicons/apple-touch-icon.png"
        val siteUrl = "https://www.codinux.net"

        val result = underTest.makeLinkAbsolute(relativeUrl, siteUrl)

        assertThat(result).isEqualTo("$siteUrl/$relativeUrl")
    }

    @Test
    fun `makeLinkAbsolute - Make relative url absolute with siteUrl that ends with a slash`() {
        val relativeUrl = "assets/favicons/apple-touch-icon.png"
        val siteUrl = "https://www.codinux.net/"

        val result = underTest.makeLinkAbsolute(relativeUrl, siteUrl)

        assertThat(result).isEqualTo("$siteUrl$relativeUrl")
    }

}