package net.dankito.utils.extensions

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.nio.ByteBuffer


class BufferExtensionsTest {

    @Test
    fun putString() {

        // given
        val buffer = ByteBuffer.allocate(256)
        val text = "To be or not to be"


        // when
        buffer.putString(text)


        // then
        assertThat(String(buffer.flipAndGetData(), Charsets.UTF_16)).isEqualTo(text)
    }

    @Test
    fun getString() {

        // given
        val buffer = ByteBuffer.allocate(256)
        val text = "To be or not to be"

        buffer.putString(text)


        // when
        val result = buffer.flipAndGetString()


        // then
        assertThat(result).isEqualTo(text)
    }

}