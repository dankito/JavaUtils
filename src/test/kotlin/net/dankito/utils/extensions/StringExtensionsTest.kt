package net.dankito.utils.extensions

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class StringExtensionsTest {

    companion object {
        private const val TestText = "Text longer than 10 characters"
    }


    @Test
    fun ofMaxLength_MaxLengthLessThenLength() {

        // when
        val result = TestText.ofMaxLength(10)

        // then
        assertThat(result.length).isEqualTo(10)
        assertThat(result).isEqualTo("Text longe")
    }

    @Test
    fun ofMaxLength_MaxLengthGreaterThenLength() {

        // when
        val result = TestText.ofMaxLength(40)

        // then
        assertThat(result.length).isEqualTo(TestText.length)
        assertThat(result).isEqualTo(TestText)
    }


    @Test
    fun ofMaxLength_MaxLengthOfZero() {

        // when
        val result = TestText.ofMaxLength(0)

        // then
        assertThat(result).isEqualTo(TestText)
    }

    @Test
    fun ofMaxLength_negativeMaxLength() {

        // when
        val result = TestText.ofMaxLength(-10)

        // then
        assertThat(result).isEqualTo(TestText)
    }

}